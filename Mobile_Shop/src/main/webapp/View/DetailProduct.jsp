<%-- 
    Document   : DetailProduct
    Created on : May 23, 2022, 8:10:43 PM
    Author     : HUNG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link
            href="https://kit-pro.fontawesome.com/releases/v5.15.4/css/pro.min.css"
            rel="stylesheet"
            />
        <title>Detail Product</title>
    </head>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        .all_page_detailProduct {
            width: 100%;
            height: auto;
        }
        .head_page_detailProduct {
            height: 120px;
            font-size: 60px;
            line-height: 120px;
            margin: 0 0 40px 0;
            background-color: rgb(222, 222, 222);
        }
        .head_title {
            width: 90%;
            margin: 0 5%;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        }
        .body_page_detailProduct {
            width: 90%;
            margin: 0 5%;
            margin-top: 2%;
            display: flex;
        }
        .body_left {
            width: 40%;
        }
        .title {
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
            margin: 10px 0 10px 0;
        }
        img {
            background-size: 80%;
        }
        .body_right {
            width: 70%;
        }
        .body_right {
            margin: 10px 0 10px 150px;
        }
        .information {
            margin: 10px 0 10px 0px;
            font-size: 15px;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        }
        .itemcode {
            display: flex;
        }
        .manufacturer {
            display: flex;
        }
        .category {
            display: flex;
        }
        .quantity {
            display: flex;
        }
        .price {
            display: flex;
        }
        .price .information {
            font-size: 25px;
        }
        .information {
            font-weight: normal;
        }
        .status {
        }
        .back {
            width: 100px;
            background-color: rgb(53, 193, 34);
            color: white;
            height: 40px;
            font-size: 20px;
            margin-right: 10px;
            border: 1px solid rgb(53, 193, 34);
            border-radius: 5px;
            cursor: pointer;
        }
        .fa-arrow-alt-left {
            color: white;
        }
        .Order_Now {
            background-color: rgb(255, 168, 17);
            color: white;
            height: 40px;
            font-size: 20px;
            border: 1px solid rgb(255, 168, 17);
            border-radius: 5px;
            cursor: pointer;
        }
        .fa-shopping-cart {
            color: white;
        }

    </style>
    <body>
        <div class="all_page_detailProduct">
            <div class="head_page_detailProduct">
                <div class="head_title">Product</div>
            </div>
            <div class="body_page_detailProduct">
                <div class="body_left">
                    <div class="image_product">
                        <img
                            src="https://cdn.hoanghamobile.com/i/preview/Uploads/2022/03/09/image-removebg-preview-3.png"
                            alt="iphone13"
                            srcset=""
                            />
                    </div>
                </div>
                <div class="body_right">
                    <div class="information_product">
                        <h3 class="title">Iphone 13</h3>
                        <p class="information">
                            Apple has officially launched the iPhone 2021 line with the name
                            iPhone 13 Series. In which, iPhone 13 Pro Max is the most advanced
                            version with a large screen, 120Hz refresh rate and up to 1TB of
                            storage.
                        </p>
                        <div class="itemcode">
                            <h3 class="title">Item Code: &nbsp</h3>
                            <p class="information">100110013009</p>
                        </div>
                        <div class="manufacturer">
                            <h3 class="title">Manufacturer: &nbsp</h3>
                            <p class="information">Apple</p>
                        </div>
                        <div class="category">
                            <h3 class="title">Category: &nbsp</h3>
                            <p class="information">
                                <a style="text-decoration: none" href="https://www.apple.com/"
                                   >Apple
                                </a>
                            </p>
                        </div>
                        <div class="quantity">
                            <h3 class="title">Available units in stock: &nbsp</h3>
                            <p class="information">800</p>
                        </div>
                        <div class="price">
                            <p class="information">1099&nbspUSD</p>
                        </div>
                        <div class="status">
                            <button class="back">
                                <i class="far fa-arrow-alt-left"></i>&nbspBack
                            </button>
                            <button class="Order_Now">
                                <i class="fas fa-shopping-cart"></i>&nbspOrder Now
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
