/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author HUNG
 */
public class Product {

    private String ProName;
    private int Price;
    private int UintsInStock;
    private String Des;
    private String Manufacturer;
    private String Category;
    private String Condition;
    private byte Image;

    public String getProName() {
        return ProName;
    }

    public void setProName(String ProName) {
        this.ProName = ProName;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public int getUintsInStock() {
        return UintsInStock;
    }

    public void setUintsInStock(int UintsInStock) {
        this.UintsInStock = UintsInStock;
    }

    public String getDes() {
        return Des;
    }

    public void setDes(String Des) {
        this.Des = Des;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String Manufacturer) {
        this.Manufacturer = Manufacturer;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String Condition) {
        this.Condition = Condition;
    }

    public byte getImage() {
        return Image;
    }

    public void setImage(byte Image) {
        this.Image = Image;
    }

    public Product(String ProName, int Price, int UintsInStock, String Des, String Manufacturer, String Category, String Condition, byte Image) {
        this.ProName = ProName;
        this.Price = Price;
        this.UintsInStock = UintsInStock;
        this.Des = Des;
        this.Manufacturer = Manufacturer;
        this.Category = Category;
        this.Condition = Condition;
        this.Image = Image;
    }

    public Product() {
    }

}
