<%-- 
    Document   : Cart
    Created on : May 23, 2022, 8:07:24 PM
    Author     : HUNG
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cart Shopping</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <link href="https://kit-pro.fontawesome.com/releases/v5.15.4/css/pro.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="/css/Cart.css">
    </head>
    <style>
        bod {
            margin: 0;
            padding: 0;
        }

        .all_page_cartShopping {
            width: 100%;
            height: auto;
        }

        .head_page_cartShopping {
            height: 150px;
            margin: 0 0 40px 0;
            background-color: rgb(222, 222, 222);
        }

        .all_head_title {
            padding-top: 1%;
        }

        .head_title {
            font-size: 50px;
            margin: 0 5%;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        }

        .head_title_p {
            margin: 0 5%;
            font-size: 18px;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        }

        .body_page_cartShopping {
            width: 80%;
            height: auto;
            margin: 0 10%;
        }

        .button_top {
            width: 100%;
            height: auto;
            display: flex;
            margin-top: 1%;
        }

        .out_button_clear {
            width: 50%;
            text-align: left;
        }

        .clearCart {
            width: 20%;
            border-radius: 5px;
            color: white;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
            background-color: rgb(225, 49, 49);
            border-color: rgb(225, 49, 49);
        }

        .out_button_checkout {
            width: 50%;
            text-align: right;
        }

        .checkOut {
            width: 21%;
            border-radius: 5px;
            color: white;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
            background-color: rgb(36, 144, 36);
            border-color: rgb(36, 144, 36);
        }

        .table_information_cart {
            width: 100%;
            height: auto;
            display: flex;
        }

        .table_all {
            width: 100%;
        }

        .th_first,
        .td_first {
            width: 40%;
            height: 40px;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        }

        .th_rest,
        .td_rest {
            width: 15%;
            height: 40px;
            font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
                "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        }

        .button_remove {
            height: 30px;
            color: white;
            background-color: rgb(225, 49, 49);
            border-color: rgb(225, 49, 49);
            border-radius: 5px;
        }

        .th_first,
        .th_rest {
            border-top: 1px solid rgb(168, 167, 167);

            border-bottom: 0.5px solid rgb(168, 167, 167);
        }

        .td_first,
        .td_rest {
            border-bottom: 0.5px solid rgb(168, 167, 167);
        }

        .button_bottom {
            margin-top: 2%;
        }

        .continue_shoping {
            border-radius: 5px;
            color: white;
            background-color: rgb(36, 144, 36);
            border-color: rgb(36, 144, 36);
        }

    </style>
    <body>
        <div class="all_page_cartShopping">
            <div class="head_page_cartShopping">
                <div class="all_head_title">
                    <div class="head_title">Cart
                    </div>
                    <p class="head_title_p">All the selected product in your cart</p>
                </div>
            </div>
            <div class="body_page_cartShopping">
                <div class="button_top">
                    <div class="out_button_clear"> <button class="clearCart"> <i class="fas fa-times"></i>&nbspClear Cart</button>
                    </div>
                    <div class="out_button_checkout"> <button class="checkOut"><i class="fas fa-shopping-cart"></i>&nbspCheck Out</button>
                    </div>
                </div>
                <div class="table_information_cart">
                    <table class="table_all">
                        <tr>
                            <th class="th_first">Product</th>
                            <th class="th_rest">Quantity</th>
                            <th class="th_rest">Unit price</th>
                            <th class="th_rest">Price</th>
                            <th class="th_rest">Action</th>
                        </tr>
                        <tr>
                            <td class="td_first">Iphone X</td>
                            <td class="td_rest">1</td>
                            <td class="td_rest">1099</td>
                            <td class="td_rest">1099</td>
                            <td class="td_rest"><button class="button_remove"><i class="fas fa-times"></i>&nbspRemove</button></td>
                        </tr>
                        <tr>
                            <th class="total_first"></th>
                            <th class="total_second"></th>
                            <th class="total">Grand Total</th>
                            <th class="totalPrice">1099</th>
                        </tr>
                    </table>

                </div>
                <div class="button_bottom">
                    <button class="continue_shoping"><i class="fas fa-arrow-left"></i>&nbspContinue Shopping</button>
                </div>
            </div>
        </div>
    </body>
</html>
